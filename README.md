# Comparaison Redmine / Gitlab

## Présentation

### Redmine

Redmine est un logiciel inspiré de GForge, la version open-source du logicial
derrière Sourceforge.net. Il est écrit en Ruby. C’est un projet uniquement
open-source et n’a aucune offre d’hébergement.

Site officiel: https://www.redmine.org/

### Gitlab

Gitlab est inspiré de Github et se veut une version libre de celui-ci. Il est
également écrit en Ruby. 

Il propose un double modèle communautaire / commercial : la version commerciale
offre un support et quelques fonctions supplémentaires. Gitlab propose également
un hébergement : gratuit et public pour les projets open-sources, payant et
optionnellement privé pour les projets propriétaires.

Site officiel : https://about.gitlab.com/

## Comparaison

### Fonctions équivalentes

#### Installation

Gitlab propose des packages complets par système d’exploitation, notamment Debian, ce qui facilite énormément la configuration et la mise-à-jour. 
L’installation de Redmine se fait manuellement et chaque nouvelle verison peut nécessiter une adaptation des modules spécifiques que nous avons mis en place (cf. plus bas).

#### Organisation et gestion des projets

Redmine propose de gérer une arborescence de projets : il y peut y avoir une relation de type père-fils entre les projets, avec héritage optionnel des membres, tickets, versions. Toutefois, les identifiants des projets sont uniques sur l’intégralité de tous les projets et sous-projets.

Gitlab propose des gérer des projets par « espace de nommage », et son idéntifés par « espace-de-nommage/nom-du-projet ». Chaque utilisateur et chaque groupe possède son propre espace de nommage, et les projets qu’il contient. Un utilisateur ne peut créer de projet que dans son espace personnel ou dans celui d’un groupe pour lequel il a la permission de le faire.

#### Visibilité des projets

Redmine ne propose que deux niveau de visibilité, configurés comme suit sur forge.irstea.fr : 

* privé : seuls les membres du projets peuvent le voir,
* public :toutes les personnes connectés à la forge ont accès à une partie du projet.

Gitlab propose trois niveaux de visibilité :

* privé : seuls les membres du projets peuvent le voir,
* interne : toutes les personnes connectés à la Gitlab ont accès à une partie du projet.
* public :tous le monde à accès à une partie du projet.

#### Gestion de tickets

Gitlab propose un système de ticket simplifié, avec des tags. Il est toutefois
possible de rediriger vers un gestionnaire de ticket externe (Mantis, Jira,
Redmine, ...), si nécessaire.

Redmine propose un système de ticket un peu plus avancé, avec la possibilité
d’avoir plusieurs workflows configurables ; malheureusement, ces workflows sont
communs à tous les projets, ce qui limite leur personnalisation.

#### Gestion de wiki

Gitalb propose un wiki utilisant le langage [Markdown](https://fr.wikipedia.org/wiki/Markdown).
Il est également possible rediriger l’utilisateur vers un wiki externe.

Redmine propose un wiki utilisant le langage [Textile]
(https://fr.wikipedia.org/wiki/Textile_(langage)).

Les deux logiciels prennent charge leur langage dans les commits et les tickets.

#### Contrôle de source et gestion des dépôts

Gitlab supporte exclusivement git, mais il est parfaitement intégré : la
création, la gestion et le contrôle d’accès des dépôts sont intégrés et
automatisé. Toutefois, un projet n’a qu’un, et un seul, dépôt git.

Redmine supporte subversion, darcs, mercurial, cvs, bazaar et git, et plusieurs
dépôts par projets ; mais tous les dépôts doivent avoir un idenfiant unique;
seuls git et subversion sont activés sur la forge. Redmine n’assure ni la
gestion ni le contrôle d’accès aux dépôts, ce doit être délégué à des logiciels
tiers. Pour forge.irstea.fr, nous avons dû récupérer et personnaliser des modules
assurant ces fonctions.

### Les plus de Gitlab

Gitlab propose un certain nombre de fonctions qui font parties des bonnes
pratiques des méthodes d e développement agiles :

####  Affichage « inline » des fichiers wiki.

Gitlab identifie et affiche en ligne les fichiers Markdown dans les sources ; il 
est possible d’y faire référence pour les afficher directement. En outre, si un 
dossier contient un fichier README.md, il est affiche directement en dessous du 
listing du dossier, et sur la page d’accueil du projet. 

Cela permet d’intégrer une documentation directement dans les sources, qui sera 
donc automatiquement versionnée.

#### Protection de branches

Gitlab permet de restreindre les modifications sur certaines branches à certains 
utilisateurs. Cela permet de s’assurer de la qualité du code et de limiter les 
modifications accidentelles.

#### Gestion des clones

Gitlab offre une fonction de « fork » : un utilisateur pour créer un clone d’un 
projet dans son espace personnel. Gitlab assure le suivi des forks et permet de 
visualiser les ramifications d’un projet initial dans ses différents forks.

#### Gestion de contributions : merge requests

Cette fonction va de pair avec la gestion de clone et la protection de branche :
un utilisateur qui n’a pas la permission de modifier un dépôt ou une branche peut
cloner un projet, modifier le clone puis proposer une contribution via une
« merge request ». Cela prend la forme d’un ticket qui permet d’étudier
l’ensemble des modifications apportés. Le responsable du dépôt ou de la branche
peut alors effectuer une revue de code, commenter, échanger avec le contributeur,
et finalement accepter ou refuser la contribution.

#### Intégration continue

Depuis sa version 8, Gitlab propose un outil d’[intégration continue](https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue) : Gitlab-CI.
Pour mémoire, l’intégration continue vise à améliorer la qualité du code et du 
produit final en effectuant une série de tâches automatisées à chaque modification 
et d’informer les développeurs des résultats le plus vite possible. Par exemple :

* compilation,
* analyse statique de code (statistiques, détection de code dupliqué, détection 
d’anti-patterns, infractions aux conventions de codage, …)
* analyse dynamique de code : tests unitaires, tests d’intégration, …
* génération de binaires,
* génération de documentation,
* déploiement.

Dans les faits, ces tâches sont prises en charge par Gitlab via des « runners ». 
Ces runners n’ont pas a être exécutés sur le même serveur de Gitlab, ce qui évite 
des problèmes de sécurité et de performances. Un utilisateur peut mettre en place 
des runners « privés » pour ses projets sans interventions de l’administrateur.

#### Webhooks

Gitlab permet d'envoyer des requêtes HTTP avec des informations lorsque certains 
événéments se produisent sur un projet (modifications apportées, création d'un tag,
fin d'un build, etc...). Ceci permet de déclencher des processus externes qui ne
peuvent pas être pris en charge en intégration continue, par exemple : la mise à
jour d'un dépôt de packages.

#### Administration d'une registry Docker

La version 8.9 de Docker permet d'intégrer une [registry Docker](https://www.docker.com/products/docker-registry).
Chaque projet peut avoir un nom associé dans la registry, avec les mêmes permissions.

En combinaison avec l'intégration continue, cela permet de fournir des images Docker
prête à utiliser pour les différentes versions du projet.

### Les plus de Redmine

#### Internationalisation

Redmine est traduit dans plusieurs langues, dont le français.

#### Champs personnalisés

Redmine permet de définir des champs personnalisés qui pourront être affectés aux
projets, tickets, ... Le type et les valeurs valides de chaque champ sont configurables. 
Il est ensuite possible de faire de recherches sur ces champs.

Sur forge.irstea.fr, ces champs sont utilisés pour stocket des méta-données sur 
les projets : UR/service responsable du projet, languages utilisés, langue, discipline
scientifique, etc...
